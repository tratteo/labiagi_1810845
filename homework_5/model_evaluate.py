
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import numpy as np

    from keras.models import model_from_json
    import tensorflow as tf
    import os
    from keras.utils import np_utils
    from keras.preprocessing.image import ImageDataGenerator
    from keras import backend as K


img_width, img_height = 64, 64

validation_data_dir = '/home/matteo/Documenti/LABIAGI/DITS-full/DITS-classification/classification train'
nb_validation_samples = 1159
batch_size = 128

def initializeParameters():
    test_datagen = ImageDataGenerator(rescale=1./ 255)
    validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='categorical')

    return validation_generator

def deserializeModel():
    json_file = open('results/model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("results/model.h5")
    print("Loaded model from disk")
    return loaded_model
    

model = deserializeModel()
model.compile(optimizer='adam',
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])
model.summary()

validation_generator = initializeParameters()

print("Evaluating model...")
score = model.evaluate_generator(validation_generator, nb_validation_samples/batch_size, workers=1)

print("Loss: ", score[0], "Accuracy: ", score[1])



