
import warnings
import numpy as np
import matplotlib
matplotlib.use("GTK3Agg")
import matplotlib.pyplot as plt

import tensorflow as tf
import os
from keras.models import load_model
from keras.layers.core import Dense, Dropout, Activation
from keras.utils import np_utils
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
warnings.filterwarnings('ignore')


img_width, img_height = 64, 64

train_data_dir = '/home/matteo/Documenti/LABIAGI/DITS-full/DITS-classification/classification train'
validation_data_dir = '/home/matteo/Documenti/LABIAGI/DITS-full/DITS-classification/classification test'
nb_train_samples = 7489
nb_validation_samples = 1159
epochs = 14
batch_size = 128


def buildModel():
    model = Sequential([
    Conv2D(16, 3, padding='same', activation='relu', input_shape=(img_height, img_width ,3)),
    MaxPooling2D(),
    Conv2D(32, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(64, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Flatten(),
    Dense(512, activation='relu'),
    Dense(59)
    ])
    return model

def initializeParameters():
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
    test_datagen = ImageDataGenerator(rescale=1./ 255)
    validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='categorical')

    train_generator = train_datagen.flow_from_directory(
        batch_size=batch_size,
        directory=train_data_dir,
        shuffle=True,
        target_size=(img_width, img_height),
        class_mode='categorical')
    return (train_generator, validation_generator)
    
def plotResults(history):
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss=history.history['loss']
    val_loss=history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.show()

def serializeModel(model):
    model_json = model.to_json()
    with open("results/model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("results/model.h5")
    print("Saved model to disk")

generators = initializeParameters()
train_generator = generators[0]
validation_generator = generators[1]

model = buildModel()
model.compile(optimizer='adam',
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])
model.summary()


history = model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size
)

serializeModel(model)
plotResults(history)


