#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <ros/ros.h>
#include <sensor_msgs/CompressedImage.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

class Listener
{
public:
        ros::Subscriber imgSub;
        void extractEdge(const sensor_msgs::CompressedImage &image)
        {
            cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);

            cv::Mat edged;
            cv::cvtColor(cv_ptr->image, edged, CV_BGR2GRAY);
            cv::GaussianBlur(edged, edged, cv::Size(3,3), 0);

            cv::Canny(edged, edged, lowThreshold, lowThreshold*ratio, kernel_size);

            cv::namedWindow( "Original", cv::WINDOW_AUTOSIZE );
            cv::imshow("Original", cv_ptr->image);

            cv::namedWindow( "Edge Detector", cv::WINDOW_AUTOSIZE );
            cv::imshow("Edge Detector", edged);
            cv::createTrackbar("Threshold", "Edge Detector", &lowThreshold, 200);
            cv::createTrackbar("Ratio", "Edge Detector", &ratio, 20);
            cv::waitKey(10);
        }
private:
    int lowThreshold = 50;
    int ratio = 5;
    int kernel_size = 3;
};

int main(int argc, char **argv)
{
    ros::init(argc, argv,"edge_extractor_node");
    Listener listener;
    ros::NodeHandle n;
    listener.imgSub = n.subscribe("/default/camera_node/image/compressed", 1, &Listener::extractEdge, &listener);
    ros::spin();
}