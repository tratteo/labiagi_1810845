#include <ros/ros.h>
#include "turtlesim/SpawnCircle.h"
#include "turtlesim/Spawn.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv,"draw_circle_client");
    if(argc != 2)
    {
        ROS_INFO("usage: spawn_circle N");
        return 1;
    }

    ROS_INFO("%d", atoi(argv[1]));
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<turtlesim::SpawnCircle>("draw_circle");
    turtlesim::SpawnCircle srv;
    srv.request.x = 10;
    for(int i = 0; i < atoi(argv[1]); i++)
    {
        srv.request.y = i + 2;
        if(client.call(srv))
        {
            ROS_INFO("Circle created and turtle spawned, %d circles", srv.response.circles.size());
        }
        else
        {
            ROS_ERROR("Unable to call SpawnCircle service");
            return 1;
        }
    }

    return 0;
}