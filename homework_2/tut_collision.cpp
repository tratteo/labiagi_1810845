#include <ros/ros.h>
#include "turtlesim/Spawn.h"
#include "turtlesim/GetCircles.h"
#include "turtlesim/Pose.h"
#include "turtlesim/RemoveCircle.h"
#include "turtlesim/Color.h"

class Listener
{
    public:
    turtlesim::Pose lastPos;
    ros::Subscriber posSub;
    ros::Subscriber colSub;
    turtlesim::Color lastColor;
    void updatePos(const turtlesim::Pose &pos)
    {
        lastPos = pos;    
    }
    void updateColor(const turtlesim::Color &col)
    {
        lastColor = col;
    }
};



int main(int argc, char **argv)
{
    int onlyRed;
    if(argc > 1)
    {
        onlyRed = std::stoi(argv[1]);
    }
    else
    {
        onlyRed = false;
    }
    
    ros::init(argc, argv,"collision_node");
    Listener listener;
    if(onlyRed) 
    {
        ROS_INFO("Detecting collision only with red circles");
    }
    else
    {
        ROS_INFO("Detecting collision with all circles");
    }
    
    ros::NodeHandle n;
    listener.posSub = n.subscribe("turtle1/pose", 1, &Listener::updatePos, &listener);
    listener.colSub = n.subscribe("turtle1/color", 1, &Listener::updateColor, &listener);
    ros::ServiceClient getSrv = n.serviceClient<turtlesim::GetCircles>("get_circle");
    turtlesim::GetCircles circles;
    ros::ServiceClient removeSrv = n.serviceClient<turtlesim::RemoveCircle>("remove_circle");
    turtlesim::RemoveCircle rmCircle;
    while(true)
    {
        if(getSrv.call(circles))
            {
                for(int i = 0; i < circles.response.circles.size(); i++)
                {
                    //ROS_INFO("%f, %f", listener.lastPos.x, listener.lastPos.y);
                    //ROS_INFO("%d, %d, %d", listener.lastColor.r, listener.lastColor.g, listener.lastColor.b);
                    if(abs(listener.lastPos.x - circles.response.circles.at(i).x) < 0.2F && abs(listener.lastPos.y - circles.response.circles.at(i).y) < 0.2F)
                    {
                        if(onlyRed)
                        {
                            if(listener.lastColor.r == 255 && listener.lastColor.g == 0 && listener.lastColor.b == 0)
                            {
                                rmCircle.request.id = circles.response.circles.at(i).id;
                                removeSrv.call(rmCircle);
                                continue;
                            }
                        }
                        else
                        {
                            rmCircle.request.id = circles.response.circles.at(i).id;
                            removeSrv.call(rmCircle);
                            continue;
                        }
                    }
                }
            }
        else
        {
            ROS_ERROR("Unable to call get_circles service, aborting");
            exit(EXIT_FAILURE);
        }
        ros::spinOnce();
    }
    
    return 0;
}