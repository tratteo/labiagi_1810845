#include <ros/ros.h>
#include "turtlesim/Spawn.h"
#include "turtlesim/RemoveCircle.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv,"remove_circle_node");
    if(argc < 2) 
    {
        ROS_ERROR("Usage: tut_remove_circle ID");
        return 1;
    }
    else if(std::stoi(argv[1]) == 1)
    {
        ROS_ERROR("You cant remove the turtle1, try with ID > 1");
        return 1;
    }

    int rid = std::stoi(argv[1]);
    ROS_INFO("%d", rid);
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<turtlesim::RemoveCircle>("remove_circle");
    turtlesim::RemoveCircle srv;
    srv.request.id = rid;
    if(client.call(srv))
    {
        ROS_INFO("Circle %d and turtle%d removed from the simulator", rid, rid);
        return 0;
    }
    else
    {
        ROS_ERROR("Unable to call remove_circle service, aborting");
        return 1;
    }
    
    return 0;
}