#include <ros/ros.h>
#include "turtlesim/SpawnCircle.h"
#include "turtlesim/Spawn.h"
#include "turtlesim/Kill.h"
#include "turtlesim/GetCircles.h"
#include "turtlesim/Circle.h"
#include "turtlesim/CircleArray.h"
#include "turtlesim/RemoveCircle.h"
#include "turtlesim/Pose.h"
#include "turtlesim/Color.h"
#include <vector>

class ServerListener
{
    public:
    ros::Publisher pub;
    std::vector<turtlesim::Circle> persistentCircles;

    bool spawnCircleCallback(turtlesim::SpawnCircle::Request &req, turtlesim::SpawnCircle::Response &res)
    {
        ros::NodeHandle nh;
        ros::ServiceClient spawnClient = nh.serviceClient<turtlesim::Spawn>("spawn");
        turtlesim::Spawn spawnSrv;
        spawnSrv.request.x = rand() % (int)req.x + 1;
        spawnSrv.request.y = rand() % (int)req.x + 1;
        spawnSrv.request.name = "turtle" + std::to_string((int)req.y);
        if(spawnClient.call(spawnSrv))
        {
            ROS_INFO("Saved Circle and spawned turtle at => (%f, %f)", spawnSrv.request.x, spawnSrv.request.y);
        }
        else
        {
            ROS_ERROR("Unable to call spawn service");
            return false;

        }
        turtlesim::Circle *c = new turtlesim::Circle();
        c->x = spawnSrv.request.x;
        c->y = spawnSrv.request.y;
        c->id = (uint8_t)req.y;
        persistentCircles.push_back(*c);
        res.circles = persistentCircles;
        return true;
    }
    bool getCirclesCallback(turtlesim::GetCircles::Request &req, turtlesim::GetCircles::Response &res)
    {
        res.circles = persistentCircles;
        return true;
    }

    bool removeCircleCallback(turtlesim::RemoveCircle::Request &req, turtlesim::RemoveCircle::Response &res)
    {
        if(!isidPresent(req.id))
        {
            return true;
        }
        else
        {
            ros::NodeHandle nh;
            ros::ServiceClient killClient = nh.serviceClient<turtlesim::Kill>("/kill");
            turtlesim::Kill killSrv;
            killSrv.request.name = "turtle"+std::to_string(req.id);
            if(killClient.call(killSrv))
            {
                int index = -1;
                for(int i = 0; i < persistentCircles.size(); i++)
                {
                    if(persistentCircles.at(i).id == req.id)
                    {
                        persistentCircles.erase(persistentCircles.begin() + i);
                        res.circles = persistentCircles;
                        return true;
                    }
                }
            }
            else
            {
                ROS_ERROR("Unable to call kill service, maybe the ID does not exists");
            }
            return true;
        }
    }
    private:
    bool isidPresent(uint8_t id)
    {
        for(int i = 0; i < persistentCircles.size(); i++)
        {
            if(persistentCircles.at(i).id == id)
                return true;
        }
        return false;
    }
    
};

int main(int argc, char **argv)
{
    ros::init(argc, argv,"draw_circle_server");
    ServerListener server;
    ros::NodeHandle n;
    ros::ServiceServer drawSrv = n.advertiseService("draw_circle", &ServerListener::spawnCircleCallback, &server);
    ros::ServiceServer getSrv = n.advertiseService("get_circle", &ServerListener::getCirclesCallback, &server);
    ros::ServiceServer rmSrv = n.advertiseService("remove_circle", &ServerListener::removeCircleCallback, &server);
    ROS_INFO("Tutorial server active...");
    ros::spin();

    return 0;
}