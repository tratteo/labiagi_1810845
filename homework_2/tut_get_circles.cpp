#include <ros/ros.h>
#include "turtlesim/Spawn.h"
#include "turtlesim/GetCircles.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv,"get_circles_node");
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<turtlesim::GetCircles>("get_circle");
    turtlesim::GetCircles srv;
    if(client.call(srv))
    {
        ROS_INFO("Service called, %d circles in the array", (srv.response.circles.size()));
        for(int i = 0; i < srv.response.circles.size(); i++)
        {
            turtlesim::Circle current = srv.response.circles.at(i);
            ROS_INFO("Circle %d => (id: %d, x: %f, y: %f)", i, current.id, current.x, current.y);
        }
    }
    else
    {
        ROS_ERROR("Unable to call get_circle service, aborting");
        return 1;
    }
    
    return 0;
}