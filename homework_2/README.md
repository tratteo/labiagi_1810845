# labiagi_1810845

Homework 2

tut_server: Nodo server fornitore dei servizi: drawCircle, getCircle, removeCircle        

tut_spawn_circles: Nodo client usato per spawnare n cerchi e n tartarughe          

tut_delete_circle: Nodo client usato per rimuovere un cerchio e una tartaruga dato un ID     

tut_get_circles: Nodo client usato per richiedere il vettore di tutti i cerchi    

tut_collision: Nodo che implementa la collisione tra le tartarughe
