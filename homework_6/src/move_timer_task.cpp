#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "move_timer_task");
    MoveBaseClient ac("move_base", true);
    while(!ac.waitForServer(ros::Duration(5.0)))
    {
        ROS_INFO("Waiting for the move_base action server to come up");
    }

    move_base_msgs ::MoveBaseGoal goal;

    goal.target_pose.header.frame_id = "base_link" ;
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = -15.277;
    goal.target_pose.pose.position.y = 23.266;
    goal.target_pose.pose.orientation.z = 1;
    goal.target_pose.pose.orientation.w = 0;
    ROS_INFO("Sending goal to move towards: %f, %f, %f", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y, goal.target_pose.pose.position.z);
    ac.sendGoal(goal);

    bool finished_before_timeout = ac.waitForResult(ros::Duration(10.0));

    if (finished_before_timeout)
    {
        actionlib::SimpleClientGoalState state = ac.getState();
        ROS_INFO("Action finished, target reached before timer, state: %s",state.toString().c_str());
    }

    else
    {
        ROS_INFO("Action time out, returning to initial pose");
        ac.cancelAllGoals();
        goal.target_pose.header.frame_id = "base_link" ;
        goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.pose.position.x = -11.277;
        goal.target_pose.pose.position.y = 23.266;
        goal.target_pose.pose.orientation.z = 1;
        goal.target_pose.pose.orientation.w = 0;
        ac.sendGoal(goal);
        ac.waitForResult();
    }
    return 0;
}